import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Layout from "components/Layout/Layout";
import HomePage from "pages/Home/Home";
import RequestPage from "pages/Request/Request";
import OrderPage from "pages/Order/Order";

const App: React.FC = () => {

  return <React.Fragment>
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={HomePage}></Route>
          <Route exact path="/request" component={RequestPage}></Route>
          <Route exact path="/order" component={OrderPage}></Route>
        </Switch>
      </Layout>
    </Router>
  </React.Fragment>;
};

export default App;
