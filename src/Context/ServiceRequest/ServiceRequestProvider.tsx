import React, { useState } from "react";
import { ServiceRequestInterface } from "interfaces/interfaces";
import ServiceRequestContext from "./ServiceRequestContext";

const ServiceRequestProvider: React.FC = props => {
  const [serviceRequest, setServiceRequest] = useState<ServiceRequestInterface>();

  return (
    <ServiceRequestContext.Provider value={{ serviceRequest, setServiceRequest }}>
      {props.children}
    </ServiceRequestContext.Provider>
  );
};

export default ServiceRequestProvider;
