import { createContext } from "react";
import { ServiceRequestInterface } from "interfaces/interfaces";

interface ServiceRequestContextInterface {
  serviceRequest?: ServiceRequestInterface
  setServiceRequest: (serviceRequest: ServiceRequestInterface) => void;
}

const ServiceRequestContext = createContext<ServiceRequestContextInterface>({
  serviceRequest: undefined,
  setServiceRequest: (serviceRequest: ServiceRequestInterface) => null,
});

export default ServiceRequestContext;
