import Card from "components/Card/Card";
import React, { Fragment } from "react";
import { Link } from "react-router-dom";

import "./Request.scss";

const RequestPage: React.FC = () => {
  return <Fragment>
    <div className="Request">
      <Card>
        <Link to="order" style={{ textDecoration: "none", color: "black" }}>Order</Link>
      </Card>
      <Card>
        <Link to="/" style={{ textDecoration: "none", color: "black" }}>Review</Link>
      </Card>
    </div>
  </Fragment>;
};

export default RequestPage;
