import Card from "components/Card/Card";
import List from "components/List/List";
import Search from "components/Search/Search";
import Update from "components/Update/Update";
import React, { Fragment, useState } from "react";

import "./Home.scss";

const HomePage: React.FC = () => {
  const [listModalIsShown, setListModalIsShown] = useState(false);
  const [searchModalIsShown, setSearchModalIsShown] = useState(false);
  const [updateModalIsShown, setUpdateModalIsShown] = useState(false);

  const toggleListModal = () => {
    setListModalIsShown(!listModalIsShown);
  };

  const toggleSearchModal = () => {
    setSearchModalIsShown(!searchModalIsShown);
  };

  const toggleUpdateModal = () => {
    setUpdateModalIsShown(!updateModalIsShown);
  };

  return <Fragment>
    <div className='home-divs'>
      {listModalIsShown ? <List onToggle={toggleListModal} /> : null}
      {searchModalIsShown ? <Search onToggle={toggleSearchModal} /> : null}
      {updateModalIsShown ? <Update onToggle={toggleUpdateModal} /> : null}
      <Card>
        <span className="componentTitle">Order</span>
        <a onClick={toggleListModal}>
          <img className='thumbnails' src="images/icon_user.png" alt="user icon" />
        </a>
      </Card>
      <Card>
        <span className="componentTitle">Track</span>
        <a onClick={toggleSearchModal}>
          <img className='thumbnails' src="images/icon_search.png" alt="F1 server status" />
        </a>
      </Card>
      <Card>
        <span className="componentTitle">Update</span>
        <a onClick={toggleUpdateModal}>
          <img className='thumbnails' src="images/icon_update.png" alt="F2 server status" />
        </a>
      </Card>
    </div>
  </Fragment >;
};

export default HomePage;
