import Card from "components/Card/Card";
import { OrganizationInterface, PatientInterface, ServiceRequestInterface } from "interfaces/interfaces";
import React, { Fragment, useState } from "react";
import { getPatient, getServiceRequestFromPatient } from "services/ServiceRequest";

import "./Order.scss";

const OrderPage: React.FC = () => {
  const [id, setId] = useState<string>("");
  const [patient, setPatient] = useState<PatientInterface>();
  const [serviceRequest, setServiceRequest] = useState<Array<ServiceRequestInterface>>([]);
  const [specimen, setSpecimen] = useState<Array<string>>(["Blood", "Tumour"]);
  const [choice, setChoice] = useState<string>("");

  const searchPatient = async (id: string): Promise<void> => {
    try {
      await getPatient(id).then((result) => {
        setPatient(result);

        getServiceRequestFromPatient(id).then((result) => {
          setServiceRequest(result);
        });
      });
    } catch (error) {
      alert("Error: " + error);
    }
  };

  const submitHandler = async (): Promise<void> => {
    try {
      await searchPatient(id);
    } catch (error) {
      alert("Error: " + error);
    }
  };

  const onOptionChangeHandler = async (event: any): Promise<void> => {
    setChoice(event.target.value);

  };

  return <Fragment>
    <div className="Order">
      <form className="order-form">
        <label>NHS id: </label>
        <input
          required
          type="string"
          name="id"
          min={1}
          placeholder="Enter patient NHS id"
          onChange={e => setId(e.currentTarget.value)}
        />
        <button className='input-button' type="button" onClick={submitHandler}>
          Enter
        </button>
      </form>
      <Card>
        <div className='patient-card'>
          <strong>Patient name: </strong>
          <span>{patient?.name[0].given[0].value} {patient?.name[0].family.value}</span>
          <br />
          <strong>Initials: </strong>
          <span>{patient?.name[0].given[0].value[0]}{patient?.name[0].family.value[0]}</span>
          <br />
          <strong>Gender: </strong>
          <span>{patient?.gender.value}</span>
          <br />
          <strong>DOB: </strong>
          <span>{patient?.birthDate.value}</span>
          <br />
          <strong>Patient NHS Id: </strong>
          <span>{patient?.identifier[0].value.value}</span>
        </div>
      </Card>
      <Card>
        <select className="dropdown" onChange={onOptionChangeHandler}>
          <option>Please choose a test for this patient</option>
          {serviceRequest.map((option, index) => {
            return <option key={index} >
              {option.code.coding[0].display.value}
            </option>;
          })}
        </select>
      </Card>
      <Card>
        <select className="dropdown" onChange={onOptionChangeHandler}>
          <option>Please choose specimen type</option>
          {specimen.map((option, index) => {
            return <option key={index} >
              {option}
            </option>;
          })}
        </select>
      </Card>
      <button>Order test</button>
    </div>
  </Fragment>;
};

export default OrderPage;
