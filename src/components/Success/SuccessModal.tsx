import React from "react";
import Modal from "../Modal/Modal";
import { CustomMessageButtonInterface } from "interfaces/interfaces";
import "./SuccessModal.scss";

const SuccessModal: React.FC<CustomMessageButtonInterface> = props => {
  return (
    <Modal onClose={props.onClose}>
      <div className="frame">
        {/* <img
          src="images/error.png"
          width="44"
          height="38"
        /> */}
        {/* <p className="title">Oh snap!</p> */}
        <p>{props.errorMessage}</p>
        <button className="button" onClick={props.onClose}>Dismiss</button>
      </div>
    </Modal>
  );
};

export default SuccessModal;
