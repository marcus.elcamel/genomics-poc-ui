import React from "react";
import { useEffect, useState } from "react";
import Card from "components/Card/Card";
import Modal from "components/Modal/Modal";
import { getPatient, getServiceRequest, getOrganization, updateServiceRequest } from "services/ServiceRequest";
import { ButtonInterface, PatientInterface, ServiceRequestInterface, OrganizationInterface } from "interfaces/interfaces";
import SuccessModal from "components/Success/SuccessModal";
import "./List.scss";

const List: React.FC<ButtonInterface> = (props) => {
  // Create a list of objects that contains "ServiceRequest, Patient, Organization" data
  // Retrieve these objects from the API
  // Map the list of objects to the UI

  const [buttonState, setButtonState] = useState(false);

  const [serviceRequests, setServiceRequests] = useState<Array<ServiceRequestInterface>>([]);
  const [patients, setPatients] = useState<Array<PatientInterface>>([]);
  const [organizations, setOrganizations] = useState<Array<OrganizationInterface>>([]);

  const [serviceRequestIds, setServiceRequestIds] = useState<Array<string>>(["fcafff6b-726b-4937-ab7e-ca45c92e6304", "6dda9172-9924-47f0-8d08-6dd4eaddcd83", "a703bac1-7ea4-4807-9b07-c9c20e17a562"]);
  const [patientIds, setPatientIds] = useState<Array<string>>(["4ecdd9d7-66ea-4d56-8c7d-c0f7864837e3", "795bb2f9-a3ec-44d3-95c7-ef6f756ede29", "f4aa1962-b25b-4915-b6fe-bd928d9c9691"]);
  const [organizationIds, setOrganizationIds] = useState<Array<string>>(["f2bf638b-da00-4b62-999c-4abb8078b9e3", "9ea46c9b-dbde-4da6-9e53-c51761e6ad02", "bc54dec2-e1e6-4c83-abd9-449b055d38ca"]);

  const successMessage = "This test is now active, please use the tracker to follow the progress of the patient.";

  try {
    const fetchData = async (): Promise<void> => {

      serviceRequestIds.forEach(async serviceRequestIds => {
        const serviceRequest: ServiceRequestInterface = await getServiceRequest(serviceRequestIds);
        setServiceRequests((serviceRequests) => {
          return [serviceRequest, ...serviceRequests];
        });
      });

      patientIds.forEach(async patientIds => {
        const patient: PatientInterface = await getPatient(patientIds);
        setPatients((patients) => {
          return [patient, ...patients];
        });
      });

      organizationIds.forEach(async organizationIds => {
        const organization: OrganizationInterface = await getOrganization(organizationIds);
        setOrganizations((organizations) => [organization, ...organizations]);
      });
    };
    console.log(serviceRequests, "ServiceRequests");
    console.log(patients, "Patients");
    console.log(organizations, "Organizations");

    useEffect(() => {
      fetchData();
    }, []);
  } catch (error) {
    console.log(error);
  }

  const toggleModal = () => {
    setButtonState(!buttonState);
  };

  // const updateStatus = async (id: string): Promise<void> => {
  //   try {
  //     await updateServiceRequest(id, "Active").then(result => {
  //       setServiceRequest(result);
  //       toggleModal();
  //     });
  //   } catch (error) {
  //     alert("Error: " + error);
  //     props.onToggle?.();
  //   }
  // };

  return (
    <Modal onClose={props.onToggle}>
      {buttonState ? (
        <SuccessModal
          onClose={toggleModal}
          errorMessage={successMessage.toString()}
        />
      ) : null}
      {/* <Card>
        <div className='patient-card'>
          <strong>Patient name: </strong>
          <span>{patient?.name[0].given[0].value} {patient?.name[0].family.value}</span>
          <br />
          <strong>DOB: </strong>
          <span>{patient?.birthDate.value}</span>
          <br />
          <strong>Patient NHS Id: </strong>
          <span>{patient?.identifier[0].value.value}</span>
          <br />
          <strong>ODS code: </strong>
          <span>{organization?.identifier[0].value.value}</span>
          <br />
          <strong>Test ID: </strong>
          <span>{serviceRequest?.id.value}</span>
          <br />
          <strong>Test: </strong>
          <span>{serviceRequest?.code.coding[0].code.value} {serviceRequest?.code.coding[0].display.value}</span>
          <br />
          <h2>Status: {serviceRequest?.status.value}</h2>
          <button className="card-buttons" onClick={e => updateStatus(serviceRequest?.id.value)} >Order test</button>
        </div>
      </Card> */}
      <div className="list-actions">
        <button className="button-alt" onClick={props.onToggle}>
          Close
        </button>
      </div>
    </Modal >
  );
};

export default List;

