import { ServiceRequestInterface } from "interfaces/interfaces";
import React, { useEffect, useState } from "react";
import "./Accordion.scss";

const Accordion: React.FC<any> = (serviceRequest) => {
  const [selected, setSelected] = useState(null);
  const [arr, setArr] = useState<Array<ServiceRequestInterface>>([]);

  useEffect(() => {
    setArr([...arr, serviceRequest.serviceRequest]);
  }, []);

  const toggle = async (id: any) => {
    if (selected === id) {
      return setSelected(null);
    }
    setSelected(id);
  };

  return (
    <div className="wrapper">
      <div className="accordion">
        {arr.map((item, index) => (
          <div className="accordion-item" key={index}>
            <div className="accordion-title" onClick={() => toggle(index)}>
              <h3>Raw FHIR</h3>
              <span> {selected == index ? "-" : "+"} </span>
            </div>
            <div className={selected == index ? "accordion-content-show" : "accordion-content"} >
              <pre>{JSON.stringify(serviceRequest, null, 2)}</pre>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Accordion;

