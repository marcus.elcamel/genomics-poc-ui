import React, { Fragment } from 'react';
import Header from 'components/Header/Header';
import './Layout.scss';
import Footer from 'components/Footer/Footer';


const Layout: React.FC = ({ children }) => {
  return (
    <Fragment>
      <Header />
      {children}
      <Footer />
    </Fragment>
  );
};

export default Layout;
