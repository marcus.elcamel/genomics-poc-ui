import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import "./Header.scss";

const Header: React.FC = () => {
  return <Fragment>
    <nav className='topnav'>
      <div className='main_image'><img src="images/icon.png" alt="Fhir icon" /></div>
      <NavLink className="links" data-testid='link-home' to="/">Genomics Diagnostic Test Ordering</NavLink>
      {/* <NavLink className="links" data-testid='link-home' to="/cats">Cats</NavLink> */}
      <div className='nhs_image'><img src="images/NHS.png" alt="NHS icon" /></div>
    </nav>
    {/* <div className='bgdiv'>
      <img className='bg' src="images/homebg.jpg"></img>
    </div> */}
  </Fragment >;
};

export default Header;
