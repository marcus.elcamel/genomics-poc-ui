/* eslint-disable linebreak-style */
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import './Modal.scss';

interface buttonInterface {
  onClose?: () => void
}

const Backdrop: React.FC<buttonInterface> = props => {
  return <div className="backdrop" onClick={props.onClose} />;
};

const ModalOverlay: React.FC = props => {
  return (
    <div className="modal">
      <div className="content">{props.children}</div>
    </div>
  );
};

const portalElement: HTMLElement = document.getElementById(
  'overlays'
) as HTMLDivElement;

const Modal: React.FC<buttonInterface> = props => {
  return (
    <Fragment>
      {ReactDOM.createPortal(<Backdrop onClose={props.onClose} />, portalElement)}
      {ReactDOM.createPortal(
        <ModalOverlay>{props.children}</ModalOverlay>,
        portalElement
      )}
    </Fragment>
  );
};

export default Modal;
