import React from "react";
import { useState } from "react";
import Card from "components/Card/Card";
import Modal from "components/Modal/Modal";
import { getPatient, getServiceRequest, getOrganization, updateServiceRequest } from "services/ServiceRequest";
import { ButtonInterface, PatientInterface, ServiceRequestInterface, OrganizationInterface } from "interfaces/interfaces";
import Dropdown from "components/Dropdown/Dropdown";
import "./Update.scss";
import SuccessModal from "components/Success/SuccessModal";

const Update: React.FC<ButtonInterface> = (props) => {
  const [id, setId] = useState<string>("");
  const [buttonState, setButtonState] = useState(false);
  const [serviceRequest, setServiceRequest] = React.useState<ServiceRequestInterface>();
  const [patient, setPatient] = React.useState<PatientInterface>();
  const [organization, setOrganization] = React.useState<OrganizationInterface>();

  const successMessage = "Update successful!";

  const submitHandler = async (): Promise<void> => {

    const searchPatient = async (id: string): Promise<void> => {
      try {
        await getPatient(id).then((result) => {
          setPatient(result);
        });
      } catch (error) {
        alert("Error: " + error);
        props.onToggle?.();
      }
    };

    const searchOrganization = async (id: string): Promise<void> => {
      try {
        await getOrganization(id).then((result) => {
          setOrganization(result);
        });
      } catch (error) {
        alert("Error: " + error);
        props.onToggle?.();
      }
    };

    try {
      await getServiceRequest(id).then(result => {
        setServiceRequest(result);
        console.log(result);

        searchPatient(result.subject.reference.value.replace("https://server.fire.ly/", "").replace("urn:uuid:", ""));

        searchOrganization(result.performer[0].reference.value.replace("https://server.fire.ly/", "").replace("urn:uuid:", ""));
      });
    } catch (error) {
      alert("Error: " + error);
      props.onToggle?.();
    }
  };

  const options = [
    { value: "Draft", label: "Draft" },
    { value: "Active", label: "Active" },
    { value: "OnHold", label: "On-hold" },
    { value: "Revoked", label: "Revoked" },
    { value: "Completed", label: "Completed" },
    { value: "EnteredInError", label: "Entered in error" },
    { value: "Unknown", label: "Unknown" }
  ];

  const updateStatus = async (status: string): Promise<void> => {
    try {
      await updateServiceRequest(id, status).then(result => {
        setServiceRequest(result);
        console.log(result);
      });
    } catch (error) {
      alert("Error: " + error + "" + id);
      props.onToggle?.();
    }
  };

  const toggleModal = () => {
    setButtonState(!buttonState);
  };

  return (
    <Modal onClose={props.onToggle}>
      {buttonState ? (
        <SuccessModal
          onClose={toggleModal}
          errorMessage={successMessage.toString()}
        />
      ) : null}
      <form className="list-form">
        <label>Service request id: </label>
        <input
          required
          type="string"
          name="id"
          min={1}
          placeholder="Enter test ID"
          onChange={e => setId(e.currentTarget.value)}
        />
        <button className='input-button' type="button" onClick={submitHandler}>
          Enter
        </button>
      </form>
      <Card>
        <div className='patient-card'>
          <strong>Patient name: </strong>
          <span>{patient?.name[0].given[0].value} {patient?.name[0].family.value}</span>
          <br />
          <strong>DOB: </strong>
          <span>{patient?.birthDate.value}</span>
          <br />
          <strong>Patient NHS Id: </strong>
          <span>{patient?.identifier[0].value.value}</span>
          <br />
          <strong>ODS code: </strong>
          <span>{organization?.identifier[0].value.value}</span>
          <br />
          <strong>Test ID: </strong>
          <span>{serviceRequest?.id.value}</span>
          <br />
          <strong>Test: </strong>
          <span>{serviceRequest?.code.coding[0].code.value} {serviceRequest?.code.coding[0].display.value}</span>
          <br />
          <h2>Status: {serviceRequest?.status.value}</h2>
          <Dropdown placeHolder="Select.." options={options} onChange={(value: string) => updateStatus(value.value).then(toggleModal)} />
        </div>
      </Card>
      <div className="update-actions">
        <button className="button-alt" onClick={props.onToggle}>
          Close
        </button>
      </div>
    </Modal>
  );
};

export default Update;

