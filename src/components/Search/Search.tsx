import React from "react";
import { useState } from "react";
import { getPatient, getServiceRequest, getOrganization, getBundleFromServiceRequest } from "services/ServiceRequest";
import { ButtonInterface, PatientInterface, ServiceRequestInterface, OrganizationInterface } from "interfaces/interfaces";
import Card from "components/Card/Card";
import Modal from "components/Modal/Modal";
import Accordion from "components/Accordion/Accordion";
import "./Search.scss";

const Search: React.FC<ButtonInterface> = (props) => {
  const [id, setId] = useState<string>("");
  const [serviceRequest, setServiceRequest] = React.useState<ServiceRequestInterface>();
  const [bundle, setBundle] = React.useState<any>(); //needs interaface
  const [patient, setPatient] = React.useState<PatientInterface>();
  const [organization, setOrganization] = React.useState<OrganizationInterface>();

  const searchPatient = async (id: string): Promise<void> => {
    try {
      await getPatient(id).then((result) => {
        setPatient(result);
      });
    } catch (error) {
      alert("Error: " + error);
      props.onToggle?.();
    }
  };

  const searchOrganization = async (id: string): Promise<void> => {
    try {
      await getOrganization(id).then((result) => {
        setOrganization(result);
      });
    } catch (error) {
      alert("Error: " + error);
      props.onToggle?.();
    }
  };

  const getBundle = async (id: string): Promise<any> => {
    try {
      await getBundleFromServiceRequest(id).then((result) => {
        setBundle(result);
      });
    } catch (error) {
      alert("Error: " + error);
      props.onToggle?.();
    }
  };

  const submitHandler = async (): Promise<void> => {
    try {
      await getServiceRequest(id).then(result => {
        setServiceRequest(result);

        searchPatient(result.subject.reference.value.replace("https://server.fire.ly/", "").replace("urn:uuid:", ""));

        searchOrganization(result.performer[0].reference.value.replace("https://server.fire.ly/", "").replace("urn:uuid:", ""));

        getBundle(id);
      });

    } catch (error) {
      alert("Error: " + error);
      props.onToggle?.();
    }
  };

  return (
    <Modal onClose={props.onToggle}>
      <form className="list-form">
        <label>Service request id: </label>
        <input
          required
          type="string"
          name="id"
          min={1}
          placeholder="Enter test ID"
          onChange={e => setId(e.currentTarget.value)}
        />
        <button className='input-button' type="button" onClick={submitHandler}>
          Enter
        </button>
      </form>
      <Card>
        <div className='patient-card'>
          <strong>Patient name: </strong>
          <span>{patient?.name[0].given[0].value} {patient?.name[0].family.value}</span>
          <br />
          <strong>DOB: </strong>
          <span>{patient?.birthDate.value}</span>
          <br />
          <strong>Patient NHS Id: </strong>
          <span>{patient?.identifier[0].value.value}</span>
          <br />
          <strong>ODS code: </strong>
          <span>{organization?.identifier[0].value.value}</span>
          <br />
          <strong>Test ID: </strong>
          <span>{serviceRequest?.id.value}</span>
          <br />
          <strong>Test: </strong>
          <span>{serviceRequest?.code.coding[0].code.value} {serviceRequest?.code.coding[0].display.value}</span>
          <br />
          <h2>Status: {serviceRequest?.status.value}</h2>
        </div>
      </Card>
      <Accordion serviceRequest={bundle!} />
      <div className="search-actions">
        <button className="button-alt" onClick={props.onToggle}>
          Close
        </button>
      </div>
    </Modal>
  );
};

export default Search;

