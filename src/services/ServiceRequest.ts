import {
  OrganizationInterface,
  PatientInterface,
  ServiceRequestInterface
} from "interfaces/interfaces";
import { get, post, put, remove } from "utilities/http-client";

export async function getServiceRequest(
  id: string
): Promise<ServiceRequestInterface> {
  try {
    const response = await get(
      `https://localhost:7116/api/ServiceRequest/${id}`
    );

    if (!response.ok) {
      return Promise.reject();
    }

    const result: ServiceRequestInterface = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function getServiceRequestFromPatient(
  id: string
): Promise<Array<ServiceRequestInterface>> {
  try {
    const response = await get(
      `https://localhost:7116/api/ServiceRequest/tests/${id}`
    );

    if (!response.ok) {
      return Promise.reject();
    }

    const result: Array<ServiceRequestInterface> = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function getPatient(id: string): Promise<PatientInterface> {
  try {
    const response = await get(`https://localhost:7116/api/Patient/${id}`);

    if (!response.ok) {
      return Promise.reject();
    }

    const result: PatientInterface = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function getOrganization(
  id: string
): Promise<OrganizationInterface> {
  try {
    const response = await get(`https://localhost:7116/api/Organisation/${id}`);

    if (!response.ok) {
      return Promise.reject();
    }

    const result: OrganizationInterface = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function updateServiceRequest(
  serviceReqId: string,
  serviceStatus: string
): Promise<ServiceRequestInterface | undefined> {
  try {
    const response = await put(
      `https://localhost:7116/api/ServiceRequest/${serviceReqId}/${serviceStatus}`
    );

    if (!response.ok) {
      return Promise.reject();
    }

    const result: ServiceRequestInterface = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function getBundleFromServiceRequest(id: string): Promise<any> {
  try {
    const response = await get(
      `https://localhost:7116/api/ServiceRequest/${id}/Bundle`
    );

    if (!response.ok) {
      return Promise.reject();
    }

    const result: any = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

// export async function createItem(item: ItemInterface): Promise<ItemInterface | undefined> {
//   try {
//     const response = await post("https://localhost:7109/api/items", item);

//     if (!response.ok) {
//       return Promise.reject();
//     }

//     const result: ItemInterface = await response.json();
//     return result;

//   } catch (error) {
//     console.log(error);
//     return Promise.reject();
//   }
// }

// export async function removeItem(id: number): Promise<ItemInterface | undefined> {
//   try {
//     const response = await remove(`https://localhost:7109/api/items/${id}`);

//     if (!response.ok) {
//       return Promise.reject();
//     }

//     const result: ItemInterface = await response.json();
//     return result;

//   } catch (error) {
//     console.log(error);
//     return undefined;
//   }
// }

// export async function editItem(item: ItemInterface): Promise<ItemInterface | undefined> {
//   try {
//     const response = await post(`https://localhost:7109/api/items/${item.id}`, item);

//     if (!response.ok) {
//       return Promise.reject();
//     }

//     const result: ItemInterface = await response.json();
//     return result;

//   } catch (error) {
//     console.log(error);
//     return Promise.reject();
//   }
// }

// export async function getItems(): Promise<Array<ServiceRequestInterface>> {
//   try {
//     const response = await get("https://localhost:7109/api/items");

//     if (!response.ok) {
//       return Promise.reject();
//     }

//     const result: Array<ItemInterface> = [...(await response.json())];
//     return result;

//   } catch (error) {
//     console.log(error);
//     return Promise.reject();
//   }
// }
