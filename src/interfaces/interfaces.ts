export interface Meta {
  versionId: string;
  lastUpdated: Date;
  profile: string[];
}

export interface Identifier {
  system: string;
  value: string;
}

export interface Coding {
  system: string;
  code: string;
  display: string;
}

export interface Code {
  coding: Coding[];
}

export interface Subject {
  reference: string;
  display: string;
}

export interface Requester {
  reference: string;
  display: string;
}

export interface Performer {
  reference: string;
  display: string;
}

export interface Name {
  use: string;
  family: string;
  given: string[];
}

export interface PatientInterface {
  resourceType: string;
  id: string;
  meta: Meta;
  identifier: Identifier[];
  name: Name[];
  gender: string;
  birthDate: string;
}

export interface Address {
  line: string[];
  city: string;
  postalCode: string;
}

export interface OrganizationInterface {
  resourceType: string;
  id: string;
  meta: Meta;
  identifier: Identifier[];
  name: string;
  address: Address[];
}

export interface ServiceRequestInterface {
  resourceType: string;
  id: string;
  meta: Meta;
  identifier: Identifier[];
  status: string;
  intent: string;
  code: Code;
  subject: Subject;
  requester: Requester;
  performer: Performer[];
}

export interface ButtonInterface {
  onToggle?: () => void;
  toggleListModal?: () => void;
  onToggleError?: () => void;
  onClick?: () => void;
  modalToggle?: boolean;
  errorMessage?: string;
}

export interface CustomMessageButtonInterface {
  onClose: () => void;
  errorMessage: string;
}
